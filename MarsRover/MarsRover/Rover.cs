﻿using System;
using static MarsRover.Constants;

namespace MarsRover
{
    public class Rover
    {      
        public int X { get; set; }
        public int Y { get; set; }
        public Direction Direction { get; set; }
        public int SurfaceHeight { get; }
        public int SurfaceWidth { get; }
        
        public Rover(int height, int width, int x, int y, Direction direction)
        {
            X = x;
            Y = y;
            Direction = direction;
            SurfaceHeight = height;
            SurfaceWidth = width;
        }
               
        //Rover move method
        public void Move(char move)
        {
            try
            {
                var movement = (Movement)Enum.Parse(typeof(Movement), move.ToString());

                switch (movement)
                {
                    case Movement.L:
                        LeftTurn();
                        break;
                    case Movement.M:
                        MoveForward();
                        break;
                    case Movement.R:
                        RightTurn();
                        break;                   
                }
            }
            catch (ArgumentException)
            {
                throw new ArgumentException("Invalid move found, valid moves are either M, L or R");
            }
            
        }       
        
        //Set Rover turns and movements
        private void LeftTurn()
        {
            switch (Direction)
            {
                case Direction.E:
                    Direction = Direction.N;
                    break;
                case Direction.N:
                    Direction = Direction.W;
                    break;
                case Direction.W:
                    Direction = Direction.S;
                    break;
                case Direction.S:
                    Direction = Direction.E;
                    break;
            }
        }

        private void RightTurn()
        {
            switch (Direction)
            {
                case Direction.E:
                    Direction = Direction.S;
                    break;
                case Direction.N:
                    Direction = Direction.E;
                    break;
                case Direction.W:
                    Direction = Direction.N;
                    break;
                case Direction.S:
                    Direction = Direction.W;
                    break;
            }
        }

        private void MoveForward()
        {
            switch (Direction)
            {
                case Direction.E:
                    if (X + 1 <= SurfaceWidth)
                        X += 1;
                    else
                        throw new ArgumentException("Rover has reached the end of the surface area");
                    break;
                case Direction.N:
                    if (Y + 1 <= SurfaceHeight)
                        Y += 1;
                    else
                        throw new ArgumentException("Rover has reached the end of the surface area");
                    break;
                case Direction.W:
                    if (X - 1 >= 0)
                        X -= 1;
                    else
                        throw new ArgumentException("Rover has reached the end of the surface area");
                    break;
                case Direction.S:
                    if (Y - 1 >= 0)
                        Y -= 1;
                    else
                        throw new ArgumentException("Rover has reached the end of the surface area");
                    break;
            }
        }
    }
}
