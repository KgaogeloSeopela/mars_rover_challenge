## Mars Rover 

This is an implementaion of the Mars Rover Challenge

---

A rover has been sent to Mars to survey the terrain. The terrain zones have been carefully surveyed ahead of time to ensure the rovers exploaration safety. 

The landing terrain is represented by a single cartesian coordinate for example (8, 8).

The rover understands the cardinal points and can face either East(E), West(W), North(N) or South(S)
at any given time. 

The rover also understands three movement commands which are 

1. M - Move one space forward in the direction it is facing.
2. R - Rotate 90 degrees to the right.
3. L - Rotate 90 degrees to the left.

---

The rover inputs are as follows

1. The first line of input requires the terrain zone boundry that the rover can move in. Eg 5 5
2. The second line of input requires the rovers starting location and dircetion. Eg 1 3 S
3. The third line of input requires a combination of commands to be executes by the rover.


And Example of the required inputs is as follows:

8 8

1 3 S

MMLLMMRRMML

The expected output for these instructions is 1 1 E

NB Please take note that no decimal points are required as part of the inputs.

---


To Run this program ~ using visual studio

1. Open the MarsRover folder
2. Double click on the MarsRover.sln to open the program files in visual studio
3. Build the solution by either pressing Ctrl + Shitf + B or right click on the project name in the solution explorer and select build 
4. Run/Start the program by either pressing F5 on your keyboard or the green arrow labelled start on your visual studio.

Code Correctness in this program was achieved by a set of unit tests which were performed on the methods.

These tests can be found in the MarsRoverTest Folder.


