﻿using System;
using static MarsRover.Constants;

namespace MarsRover
{
    class Program
    {
        static void Main(string[] args)
        {
            IFormatInputs _inputFormat = new FormatInputsHelper();

            //Getting user inputs
            Console.Write("Enter landing terrain bound - eg 5 5: ");
            var terrain = Console.ReadLine();

            Console.Write("Enter the starting location and orientation - eg 1 2 E: ");
            var startingLocation = Console.ReadLine();

            Console.Write("Enter the list of commands - eg MLR: ");
            var commands = Console.ReadLine();

            //Landing terrain ~ split into an array
            var terrainArray = terrain.Split(' ');
            if (terrainArray.Length != 2)
                throw new ArgumentOutOfRangeException("Incorrect values entered");

            var width = _inputFormat.GetIntConversion(terrainArray[0]);
            var height = _inputFormat.GetIntConversion(terrainArray[1]);


            //starting location
            var locationArray = startingLocation.Split(' ');
            if (locationArray.Length != 3)
                throw new ArgumentOutOfRangeException("Incorrect values entered");

            var xLocation = _inputFormat.GetIntConversion(locationArray[0]);
            var yLocation = _inputFormat.GetIntConversion(locationArray[1]);
            var initialDirection = _inputFormat.GetConvertedDirection(locationArray[2].ToUpper());

            commands = _inputFormat.ReplaceStringSpace(commands);

            CalculateNewRoverPosition(width, height, xLocation, yLocation, initialDirection, commands);

            Console.ReadLine();
        }

        private static void CalculateNewRoverPosition(int width, int height, int xLoc, int yLoc, Direction dir, string commands)
        {
            Rover MarsRover = new Rover(width, height, xLoc, yLoc, dir);

            foreach (var command in commands)
            {
                MarsRover.Move(command);
            }

            var x = MarsRover.X;
            var y = MarsRover.Y;
            var direction = MarsRover.Direction;

            Console.WriteLine("The rover is now at position: {0} {1} {2}", x, y, direction);
        }
    }
}
