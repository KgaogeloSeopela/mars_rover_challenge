﻿using System;
using System.Text.RegularExpressions;
using static MarsRover.Constants;

namespace MarsRover
{
    public class FormatInputsHelper : IFormatInputs
    {
        public Direction GetConvertedDirection(string direction)
        {
            try
            {
                return (Direction)Enum.Parse(typeof(Direction), direction);
            }
            catch (ArgumentException)
            {
                throw new ArgumentException("Invalid direction entered");
            }
        }

        public int GetIntConversion(string strValue)
        {
            try
            {
                return int.Parse(strValue);
            }
            catch (FormatException)
            {
                throw new FormatException("Invalid value entered");
            }
        }

        public string ReplaceStringSpace(string strValue)
        {
            return Regex.Replace(strValue, @"\s+", "").ToUpper();
        }
    }
}
