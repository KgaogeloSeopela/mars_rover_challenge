﻿namespace MarsRover
{
    public static class Constants
    {
        public enum Direction
        {
            N,
            E,
            W,
            S
        }

        public enum Movement
        {
            L,
            R,
            M
        }
    }
}
