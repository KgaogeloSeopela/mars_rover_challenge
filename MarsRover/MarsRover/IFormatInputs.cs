﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static MarsRover.Constants;

namespace MarsRover
{
    public interface IFormatInputs
    {
        string ReplaceStringSpace(string strValue);
        int GetIntConversion(string strValue);
        Direction GetConvertedDirection(string direction);
    }
}
