﻿using MarsRover;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using static MarsRover.Constants;

namespace MarsRoverTest
{
    [TestClass]
    public class FormatInputHelperTest
    {
        [TestMethod]
        public void GetDirection_WithInvalid_Direction()
        {
            //Checks if method throws exception due to incorrect direction
            FormatInputsHelper inputFormat = new FormatInputsHelper();
            Assert.ThrowsException<ArgumentException>(() => inputFormat.GetConvertedDirection("T"));
        }

        [TestMethod]
        public void GetDirection_WithValid_Direction()
        {
            //Checks if method returns the formatted direction
            FormatInputsHelper inputFormat = new FormatInputsHelper();
            Assert.AreEqual(Direction.E, inputFormat.GetConvertedDirection("E"));
        }

        [TestMethod]
        public void GetIntConversion_WithInValid_String()
        {
            //Checks if the method throws an exception when int conversion fails
            FormatInputsHelper inputFormat = new FormatInputsHelper();
            Assert.ThrowsException<FormatException>(() => inputFormat.GetIntConversion("A"));
        }

        [TestMethod]
        public void GetIntConversion_WithValid_String()
        {
            //Checks if the method correctly converts string to int
            FormatInputsHelper inputFormat = new FormatInputsHelper();
            var convertedInt = inputFormat.GetIntConversion("1");
            Assert.AreEqual(1, convertedInt);
        }

        [TestMethod]
        public void ReplaceStringSpaceTest()
        {
            //Checks if the method removes spaces in a string and takes it to uppercase
            FormatInputsHelper inputFormat = new FormatInputsHelper();
            var inputString = inputFormat.ReplaceStringSpace("mm MN 123 uuu");
            Assert.AreEqual("MMMN123UUU", inputString);
        }
    }
}
