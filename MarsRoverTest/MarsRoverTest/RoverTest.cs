﻿using MarsRover;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using static MarsRover.Constants;

namespace MarsRoverTest
{
    [TestClass]
    public class RoverTest
    {
        [TestMethod]
        public void Move_WithInvalid_Movement()
        {
            //Checks if method throws exception
            Rover rover = new Rover(8, 8, 1, 2, Direction.E);
            Assert.ThrowsException<ArgumentException>(() => rover.Move('T'));
        }

        [TestMethod]
        public void Move_WithValid_Turns()
        {
            //Checks if the rover made the correct right and left turns -- RightTurn() and LeftTurn() Methods
            Rover rover = new Rover(8, 8, 1, 2, Direction.S);

            rover.Move('L');            
            Assert.AreEqual(Direction.E, rover.Direction);            

            rover.Move('R');
            Assert.AreEqual(Direction.S, rover.Direction);
        }

        [TestMethod]
        public void Move_UpWithInValid_Movements()
        {
            //Checks if the method throws an exception when moving out of defined Y bounds
            Rover rover = new Rover(8, 8, 8, 8, Direction.N);
            Assert.ThrowsException<ArgumentException>(() => rover.Move('M'));
        }

        [TestMethod]
        public void Move_RightWithInValid_Movements()
        {
            //Checks if the method throws an exception when moving out of defined X bounds -- MoveForward() method
            Rover rover = new Rover(8, 8, 8, 8, Direction.E);
            Assert.ThrowsException<ArgumentException>(() => rover.Move('M'));
        }

        [TestMethod]
        public void Move_LeftWithInValid_Movements()
        {
            //Checks if the method throws an exception when moving into negative Y area bounds -- MoveForward() method
            Rover rover = new Rover(8, 8, 8, 0, Direction.S);
            Assert.ThrowsException<ArgumentException>(() => rover.Move('M'));
        }

        [TestMethod]
        public void Move_DownWithInValid_Movements()
        {
            //Checks if the method throws an exception when moving into negative X area bounds -- MoveForward() method
            Rover rover = new Rover(8, 8, 0, 8, Direction.W);
            Assert.ThrowsException<ArgumentException>(() => rover.Move('M'));
        }

        [TestMethod]
        public void Move_WithValid_Movements()
        {
            //Checks if rover moves correctly -- MoveForward() method
            Rover rover = new Rover(8, 8, 8, 7, Direction.N);
            rover.Move('M');
            Assert.AreEqual(8, rover.Y);            
        }

    }
}
